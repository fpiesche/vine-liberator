# README #

Quick and dirty Python script to download all Vine videos for a given user as .mp4 files
in the highest available quality. Also saves your entire timeline info as a .json file.

Report any problems using the "Issues" section over on the left, please!

### User Guide ###

#### I Know Python, Give Me The Quick Summary ####

* Install the `requests` library using `pip` or such
* Download the script
* Run the script with `python vine-liberator.py MyVineUsername` to download your videos.

#### Windows ####

* Download and install `miniconda` from http://conda.pydata.org/miniconda.html - you
  probably want the 32-bit Python 2.7 installer here.
* At the end of the Miniconda installation, a command-line window should automatically open.
* In this window, enter `conda install requests` to install the one external Python module required
* Save vine-liberator.py somewhere you can find it, such as `c:\Users\MyName\Downloads\vine-liberator.py`. Either use the full download in the "Downloads" section on the left and unzip it, or navigate to `Source` > `vine-liberator.py`, click `Raw` at the top and save the file.
* In the miniconda commandline window, enter `cd /d c:\Users\MyName\Downloads\` (you can press `Tab` after each `\` to cycle through your options)
* Once you're in the directory holding `vine-liberator.py`, run `python vine-liberator.py MyVineUsername` to download your videos.

#### MacOS and Linux ####

* Save vine-liberator.py somewhere you can find it, such as your `Downloads` folder.  Either use the full download in the "Downloads" section on the left and unzip it, or navigate to `Source` > `vine-liberator.py`, click `Raw` at the top and save the file.
* Open the Terminal app (or your Linux distribution's equivalent)
* In your terminal, navigate to where you've saved the script, e.g. using `cd ~/Downloads` if it's in your Downloads folder.
* Run `sudo pip install requests` to install the required Python module - you will need to enter your normal login password for this to work.
* Run the script using `python vine-liberator.py MyVineUsername` to download your videos.