from __future__ import print_function
import argparse
import json
import os
import re
import unicodedata
import requests


def slugify(description):
    # shamelessly stolen from Django
    value = unicodedata.normalize('NFKD', description).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)


parser = argparse.ArgumentParser(description='Download all vine videos for the given user name.')
parser.add_argument('user', help='The user name to download videos for.')
parser.add_argument('--ignoremissing', '-i', action='store_true',
                    help='Download all videos found even if there appear to be some missing.')
parser.add_argument('--debug', '-d', action='store_true',
                    help='Save timeline pages as separate JSON files and output number of entries per page.')
args = parser.parse_args()
opts = vars(args)

profile_api_url = 'https://vine.co/api/users/profiles/vanity/%s' % opts['user']
download_path = '%s_vine_downloads' % opts['user']
user_id = json.loads(requests.get('%s' % profile_api_url).text)['data']['userId']

timeline_api_url = 'https://vine.co/api/timelines/users/%s' % user_id

print('Getting timeline for user %s (ID %s)...' % (opts['user'], user_id))
page = 1
timeline = json.loads(requests.get('%s?page=%s' % (timeline_api_url, page)).text)

if opts['debug']:
    print('\tPage %s has %s entries...' % (page, len(timeline['data']['records'])))
    print('\t\t%s' % '%s?page=%s' % (timeline_api_url, page))
    with open(os.path.join(download_path, 'timeline_page%s.json' % page), 'w') as json_file:
        print('Saving timeline data to timeline_page%s.json.' % page)
        json_file.write(json.dumps(timeline))

while True:
    page += 1
    url = '%s?page=%s' % (timeline_api_url, page)
    page_data = json.loads(requests.get(url).text)
    if len(page_data['data']['records']) == 0:
        break
    else:
        got_entries = [entry['postId'] for entry in timeline['data']['records']]
        if opts['debug']:
            print('\tPage %s has %s entries...' % (page, len(page_data['data']['records'])))
            print('\t\t%s' % url)
            with open(os.path.join(download_path, 'timeline_page%s.json' % page), 'w') as json_file:
                print('Saving timeline data to timeline.json.')
                json_file.write(json.dumps(page_data))
        new_entries = [entry for entry in page_data['data']['records'] if entry['postId'] not in got_entries]
        timeline['data']['records'] += new_entries

if not os.path.exists(download_path):
    os.makedirs(download_path)

my_videos = [entry for entry in timeline['data']['records'] if entry['userId'] == user_id]

print('Found %s videos on timeline for %s (%s from this user).' %
      (len(timeline['data']['records']), opts['user'], len(my_videos)))

if len(timeline['data']['records']) != timeline['data']['count']:
    print('WARNING: Timeline stats suggest %s videos on timeline but only managed to find %s.' %
          (timeline['data']['count'], len(timeline['data']['records'])))
    print('It\'s likely that the missing entries are videos/reposts that have been deleted.')
    print('If there are videos missing, please get in touch with me (@ektoutie on Twitter) to investigate.')
    if not opts['ignoremissing']:
        print('To download the %s videos we can see, start this script again and add the \'--ignoremissing\' parameter.'
              % len(timeline['data']['records']))
        exit(1)

for entry in timeline['data']['records']:
    if entry['userId'] != user_id:
        print('Skipping reposted video %s.' % entry['postId'])
        continue
    timestamp = entry['created'].replace(':', '').replace('.', '')
    if entry['description'] != '':
        filename = os.path.join(download_path, '%s-%s.mp4' % (timestamp, slugify(entry['description'])))
    else:
        filename = os.path.join(download_path, '%s.mp4' % (timestamp))

    with open(filename, 'wb') as video_file:
        video_url = entry['videoDashUrl'] if entry['videoDashUrl'] is not None else entry['videoUrl']
        print('Downloading video %s from %s.' % (entry['postId'], timestamp))
        request = requests.get(video_url, stream=True)
        for chunk in request.iter_content(chunk_size=1024):
            if chunk:
                video_file.write(chunk)

with open(os.path.join(download_path, 'timeline.json'), 'w') as json_file:
    print('Saving timeline data to timeline.json.')
    json_file.write(json.dumps(timeline))
